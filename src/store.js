import Vue from 'vue'
import Vuex from 'vuex'

import dashboardStore from './components/dashboard/dashboardStore'
import userStore from './components/user/userStore'
import chatStore from './components/chat/chatStore'
import privateMessageStore from './components/private-message/privateMessageStore'

Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    dashboardStore, userStore, chatStore, privateMessageStore
  },
  strict: debug
})
