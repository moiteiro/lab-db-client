import Vue from 'vue'
import {getHeader} from './../../config'

const state = {
  dashboard: {}
}

const mutations = {
  SET_DASHBOARD_STATISTICS (state, statistics) {
    state.dashboard = statistics
  }
}

const actions = {
  setDashboard: ({commit}) => {
    return Vue.http.get('http://localhost:8000/api/v1/get-statistics', {headers: getHeader()})
      .then(response => {
        commit('SET_DASHBOARD_STATISTICS', response.body.data[0])
      })
  }
}

export default {
  state, mutations, actions
}
